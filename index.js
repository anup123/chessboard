
// Write Javascript code!
const appDiv = document.getElementById('chess');
appDiv.innerHTML = `<h1>Chess Board</h1>`;

document.addEventListener('click', function (event) {
    event.preventDefault();
    let diagonals = getDiagonals(event.target.className);
    resetBoard();
    paintRed(diagonals);

}, false);

function getDiagonals(cell) {
    let rowIndex = ['1', '2', '3', '4', '5', '6', '7', '8'];
    let columnIndex = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    let currentCoordinate = cell.split('').map(e => {
        if (columnIndex.indexOf(e) !== -1) {
            return columnIndex.indexOf(e) + 1;
        } else if (rowIndex.indexOf(e) !== -1) {
            return rowIndex.indexOf(e) + 1;
        }
        return e
    })
    let x = currentCoordinate[0];
    let y = currentCoordinate[1];
    let diagTop = [];
    let diagBottom = [];
    diagTop.push(cell);
    // BOTTOM DIAGONALS
    for (let i = 1; y + i < 9; i++) {
        if (columnIndex[x + i - 1] !== undefined) diagTop.push(`${columnIndex[x + i - 1]}${y + i}`)
    }

    for (let i = 1; y - i >= 1; i++) {
        if (columnIndex[x + i - 1] !== undefined) diagTop.push(`${columnIndex[x + i - 1]}${y - i}`)
    }

    // TOP DIAGONALS
    for (let i = 1; x - i >= 1; i++) {
        if (columnIndex[x - i - 1] !== undefined) diagBottom.push(`${columnIndex[x - i - 1]}${y - i}`);
    }

    for (let i = 1; x + i < 9; i++) {
        if (columnIndex[x - i - 1] !== undefined) diagBottom.push(`${columnIndex[x - i - 1]}${y + i}`);
    }
    return diagTop.concat(diagBottom);
}

function paintRed(cells) {
    console.log("Paint red is called on", cells);
    cells.forEach(cell => {
        let domElement = document.getElementsByClassName(cell);
        //domElement[0].style.backgroundColor = "red";
        domElement[0].classList.add('red');
    });
}

function resetBoard() {
    let redSquares = document.getElementsByClassName('red');
    if (redSquares.length > 0) {
        Array.from(redSquares).forEach((redSquare) => {
            redSquare.classList.remove('red');
        });
    }
}
